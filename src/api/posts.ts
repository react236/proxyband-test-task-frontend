import { getRequest } from './network'

export const postRequest = async (id: string) => {
    let url = `/posts/${id}`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const postsRequest = async (user: string | null) => {
    if (user === null) return []

    let url = `/users/${user}/posts`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
