import { userRequest, usersRequest } from './users'
import { postRequest, postsRequest } from './posts'
import { albumsRequest, photosRequest, photoRequest } from './albums'

export { 
    userRequest,
    usersRequest, 
    postsRequest,
    postRequest,
    albumsRequest,
    photosRequest,
    photoRequest,
}
