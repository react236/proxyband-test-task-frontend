import { getRequest } from './network'

export const userRequest = async (id, token = '') => {
    let url = `/users/${id}`

    const result = await getRequest(url, token, true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const usersRequest = async () => {
    let url = '/users'

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
