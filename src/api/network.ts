import axios from "axios"

import { errorMessage, errorToast, noData } from "./helpers"
import { options } from './options'

const backendURL = 'https://jsonplaceholder.typicode.com'

const request = async (method: any, url: string, body: any, token = '', spinner: boolean = false) => {
    const fullUrl = backendURL + url

    let result

    try {
        if (body) {
            result = await method(fullUrl, body, options)
        } else {
            result = await method(fullUrl, options)
        }
    } catch (e: any) {
        result = errorMessage(e)
    } finally {
    }
    
    if (result) {
        if ('data' in result) {

            if (typeof result.data === 'object' && 'error' in result.data) {
                errorToast(result.data.error)
            }

            if (typeof result.data === 'string') {
                return {}
            }
            
            return result.data
        }
    }
    
    return noData
}

export const getRequest = async (url: string, token = '', spinner: boolean = false) => {
    const result = await request(axios.get, url, null, token, spinner)
    return result
}

export const postRequest = async (url: string, body: any, token: string = '', spinner: boolean = false) => {
    const result = await request(axios.post, url, body, token, spinner)
    return result
}

export const patchRequest = async (url: string, body: any, token: string = '', spinner: boolean = false) => {
    const result = await request(axios.patch, url, body, token, spinner)
    return result
}

export const deleteRequest = async (url: string, token: string = '', spinner: boolean = false) => {
    const result = await request(axios.patch, url, null, token, spinner)
    return result
}
