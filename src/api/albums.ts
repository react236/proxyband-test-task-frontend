import { getRequest } from './network'

export const photosRequest = async (id: string) => {
    let url = `/album/${id}/photos`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const albumsRequest = async (user: string | null) => {
    if (user === null) return []

    let url = `/users/${user}/albums`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}

export const photoRequest = async (id: string) => {
    let url = `/photos/${id}`

    const result = await getRequest(url, '', true)

    if ( !('error' in result) ) {
        return result
    }

    return []
}
