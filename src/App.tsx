import React from "react"
import { createBrowserRouter, RouterProvider } from "react-router-dom"

import Root from './routes/root'
import Users from './routes/users'
import Posts from "./routes/posts"
import Post from "./routes/post"
import Albums from "./routes/albums"
import Photos from "./routes/photos"
import Photo from "./routes/photo"

import { loader as usersLoader } from './components/users/users'
import { loader as postsLoader } from './components/posts/posts'
import { loader as postLoader } from './components/post/post'
import { loader as albumsLoader } from './components/albums/albums'
import { loader as photosLoader } from './components/photos/photos'
import { loader as photoLoader } from './components/photo/photo'

import { Spinner } from './components/spinner/spinner'

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root />,
        children: [
            {
                children: [
                    {
                        index: true,
                        element: <Users />,
                        loader: usersLoader,
                    },
                    {
                        path: "users",
                        element: <Users />,
                        loader: usersLoader,
                    },
                    {
                        path: "posts/:userId",
                        element: <Posts />,
                        loader: postsLoader,
                    },
                    {
                        path: "post/:postId",
                        element: <Post />,
                        loader: postLoader,
                    },
                    {
                        path: "albums/:userId",
                        element: <Albums />,
                        loader: albumsLoader,
                    },
                    {
                        path: "photos/:albumId",
                        element: <Photos />,
                        loader: photosLoader,
                    },
                    {
                        path: "photo/:photoId",
                        element: <Photo />,
                        loader: photoLoader,
                    },
                ],
            },
        ],
    },
]);

const App = () => {
    return (
        <RouterProvider
            router={router}
            fallbackElement = {(
                <div style={{ position: 'relative' }}>
                    <Spinner enable={true} />
                </div>
            )}
        />
    )
}

export default App
