import React from 'react'

import styles from './button.module.css'

interface iProps {
    text: string
    onClick: () => void
}

export const Button: React.FC<iProps> = ({ text, onClick }) => {
    return (
        <button className={styles.container} onClick={onClick}>{text}</button>
    )
}
