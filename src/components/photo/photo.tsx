import React from "react"
import { useLoaderData, useNavigate } from "react-router"

import { photoRequest } from "../../api"

import { iPhoto } from "../../interfaces"

import styles from './photo.module.css'

interface loaderData {
    photo: iPhoto
}

export async function loader({ params }) {
    const photoId = params.photoId

    let photo = await photoRequest(photoId)
    if ('error' in photo) {
        photo = []
    }

    return { photo }
}

export const Photo = () => {
    const navigate = useNavigate()
    const { photo } = useLoaderData() as loaderData

    const handleClose = () => {
        navigate(-1)
    }

    return (
        <div className={styles.container}>
            <div className={styles.close} onClick={handleClose}>X</div>
            <div className={styles.imgContainer}>
                <img className={styles.img} src={photo.url} alt={photo.title} />
                <div className={styles.title}>{photo.title}</div>
            </div>
        </div>
    )
}
