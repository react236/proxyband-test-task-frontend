import React from 'react'
import { useNavigate } from 'react-router'

import styles from './back.module.css'

export const Back = () => {
    const navigate = useNavigate()

    const handleBack = () => {
        navigate(-1)
    }

    return (
        <button className={styles.container} onClick={handleBack}>Назад</button>
    )
}
