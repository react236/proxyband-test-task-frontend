import React from "react"
import { useNavigate } from "react-router"

import { iUser } from "../../interfaces"
import { Button } from "../button/button"

import styles from './user.module.css'

interface iProps {
    user: iUser
}

export const User: React.FC<iProps> = ({ user }) => {
    const navigate = useNavigate()

    const handlePosts = () => {
        navigate(`/posts/${user.id}`)
    }

    const handleAlbums = () => {
        navigate(`/albums/${user.id}`)
    }

    return (
        <div className={styles.container}>
            <div className={styles.name}><b>{user.name}</b></div>
            <div className={styles.email}>{user.email}</div>
            <div className={styles.username}><b>{user.username}</b></div>
            <table>
                <tbody>
                    <tr>
                        <td className={styles.firstColumn}><b>Address:</b></td>
                    </tr>
                    <tr>
                        <td>{user.address.street}</td>
                        <td>{user.phone}</td>
                    </tr>
                    <tr>
                        <td>{user.address.suite}</td>
                        <td>{user.website}</td>
                    </tr>
                    <tr>
                        <td>{user.address.city}</td>
                    </tr>
                    <tr>
                        <td>{user.address.zipcode}</td>
                    </tr>
                </tbody>
            </table>
            <div className={styles.actions}>
                <Button text='Posts' onClick={handlePosts} />
                <Button text='Albums' onClick={handleAlbums} />
            </div>
        </div>
    )
}
