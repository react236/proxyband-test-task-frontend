import React from "react"
import { useLoaderData } from 'react-router'
import { Link } from "react-router-dom"

import { Back } from "../back/back"
import { postsRequest } from '../../api'

import { iPost } from '../../interfaces'

import styles from './posts.module.css'

interface loaderData {
    posts: iPost[]
}

export async function loader({ params, request }) {
    const userId = params.userId

    let posts = await postsRequest(userId)
    if ('error' in posts) {
        posts = []
    }

    return { posts }
}

export const Posts = () => {
    const { posts } = useLoaderData() as loaderData

    return (
        <div className={styles.container}>
            {posts.map(post => (
                <div key={post.id}>
                    <Link to={`/post/${post.id}`}>
                        {post.title}
                    </Link>
                </div>
            ))}
            <Back />
        </div>
    )
}
