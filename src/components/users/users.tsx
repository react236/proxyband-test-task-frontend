import React from "react"
import { useLoaderData, useParams } from 'react-router'
import { Form, useSubmit } from "react-router-dom"
import sortBy from 'sort-by'

import { User } from '../user/user'
import { usersRequest } from '../../api'

import { iUser } from '../../interfaces'

import styles from './users.module.css'

interface loaderData {
    users: iUser[]
    search: string
    sort: string
}

export async function loader({ request }) {
    const url = new URL(request.url)
    const search = url.searchParams.get("search")
    const sort = url.searchParams.get("sort")
    
    let usersAll = await usersRequest()
    if ('error' in usersAll) {
        usersAll = []
    }

    const usersUnsorted = search ? usersAll.filter( (item: iUser) => item.username.includes(search) ) : usersAll
    let users: iUser[]
    switch (sort) {
        case 'asc':
            users = usersUnsorted.sort(sortBy('username'))
            break
        case 'desc':
            users = usersUnsorted.sort(sortBy('-username'))
            break;
        case 'none':
        default:
            users = usersUnsorted
            break;
    }

    return { users, search, sort }
}

export const Users = () => {
    const { users, search, sort } = useLoaderData() as loaderData
    const submit = useSubmit()

    const handleChange = (e: React.FormEvent) => {
        submit(e.currentTarget as HTMLInputElement)
    }

    return (
        <div className={styles.container}>
            <div className={styles.search}>
                <Form id='search-form' onChange={e => handleChange(e)}>
                    <input 
                        id="search"
                        name="search"
                        type="search"
                        defaultValue={search}
                        placeholder="Username"
                        aria-label="Search"
                    />
                    <label className={styles.sortLabel}>
                        Сортировка:
                        <select id='sort' name='sort' defaultValue={sort === null ? 'none' : sort}>
                            <option value='asc'>ASC</option>
                            <option value='desc'>DESC</option>
                            <option value='none'>None</option>
                        </select>
                    </label>
                </Form>
            </div>
            <div className={styles.users}>
                {users.map(user => (
                    <User key={user.username} user={user} />
                ))}
            </div>
        </div>
    )
}
