import React from "react"
import { useLoaderData } from "react-router"

import { Back } from '../back/back'
import { postRequest } from "../../api"

import { iPost } from "../../interfaces"

import styles from './post.module.css'

interface loaderData {
    post: iPost
}

export async function loader({ params }) {
    const postId = params.postId

    let post = await postRequest(postId)
    if ('error' in post) {
        post = []
    }

    return { post }
}

export const Post = ()  => {
    const { post } = useLoaderData() as loaderData

    return (
        <>
            <article className={styles.container}>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
            </article>
            <Back />
        </>
    )
}
