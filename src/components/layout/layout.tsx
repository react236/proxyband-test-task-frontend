import React, { ReactElement } from 'react'
import { useNavigation } from 'react-router-dom'
import { Spinner } from '../spinner/spinner'

import styles from './layout.module.css'

interface iProps {
    children: ReactElement[]
}

export const Layout: React.FC<iProps> = ({ children }) => {
    const navigation = useNavigation()

    return (
        <div className={styles.container}>
            <div className={styles.data}>
                {children}
            </div>
            <Spinner enable={navigation.state === 'loading' || navigation.state === 'submitting'} />
        </div>
    )
}
