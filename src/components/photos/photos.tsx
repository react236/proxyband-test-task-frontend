import React from "react"
import { useLoaderData } from "react-router"
import { Link } from "react-router-dom"

import { Back } from '../back/back'
import { photosRequest } from "../../api"

import { iPhoto } from "../../interfaces"

import styles from './photos.module.css'

interface loaderData {
    photos: iPhoto[]
}

export async function loader({ params }) {
    const albumId = params.albumId

    let photos = await photosRequest(albumId)
    if ('error' in photos) {
        photos = []
    }

    return { photos }
}

export const Photos = ()  => {
    const { photos } = useLoaderData() as loaderData

    return (
        <>
            <div className={styles.container}>
                {photos.map(photo => (
                    <div key={photo.id}>
                        <Link to={`/photo/${photo.id}`}>
                            <div className={styles.thumb}>
                                <img src={photo.thumbnailUrl} alt='thumb' />
                                <div className={styles.title}>{photo.title}</div>
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
            <Back />
        </>
    )
}
