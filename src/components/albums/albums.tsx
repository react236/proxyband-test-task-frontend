import React from "react"
import { useLoaderData } from 'react-router'
import { Link } from "react-router-dom"

import { Back } from "../back/back"
import { albumsRequest } from '../../api'

import { iAlbum } from '../../interfaces'

import styles from './albums.module.css'

interface loaderData {
    albums: iAlbum[]
}

export async function loader({ params }) {
    const userId = params.userId

    let albums = await albumsRequest(userId)
    if ('error' in albums) {
        albums = []
    }

    return { albums }
}

export const Albums = () => {
    const { albums } = useLoaderData() as loaderData

    return (
        <div className={styles.container}>
            {albums.map(album => (
                <div key={album.id}>
                    <Link to={`/photos/${album.id}`}>
                        {album.title}
                    </Link>
                </div>
            ))}
            <Back />
        </div>
    )
}
