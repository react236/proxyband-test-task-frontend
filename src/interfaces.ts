export interface iUser {
    id: number
    name: string
    username: string
    email: string
    phone: string
    website: string
    address: {
        street: string
        suite: string
        city: string
        zipcode: string
    }
    geo: {
        lat: string
        lng: string
    }
    company:	{
        name: string
        catchPhrase: string
        bs: string
    }
}

export interface iPost {
    userId: number
    id: number
    title: string
    body: string
}

export interface iAlbum {
    userId: number
    id: number
    title: string
}

export interface iPhoto {
    albumId: number
    id: number
    title: string
    url: string
    thumbnailUrl: string
}
